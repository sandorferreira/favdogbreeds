//
//  DogCollectionViewCell.swift
//  FavDogBreeds
//
//  Created by Sandor ferreira da silva on 23/01/19.
//  Copyright © 2019 Sandor Ferreira da Silva. All rights reserved.
//

import UIKit

public class DogCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var dogImageView: UIImageView!
    
}
