//
//  DogTableViewCell.swift
//  FavDogBreeds
//
//  Created by Sandor ferreira da silva on 23/01/19.
//  Copyright © 2019 Sandor Ferreira da Silva. All rights reserved.
//

import UIKit

protocol FavoriteButtonDelegate {
    func favoriteBreedTapped(at index: IndexPath)
}

public class DogTableViewCell : UITableViewCell {
    var delegate: FavoriteButtonDelegate!
    var indexPath: IndexPath!
    
    @IBOutlet weak var customContentView: UIView!
    
    @IBOutlet weak var dogImageView: UIImageView!
    
    @IBOutlet weak var breedNameLabel: UILabel!
    
    @IBOutlet weak var buttonFav: UIButton!
    
    @IBOutlet weak var numberOfPhotosLabel: UILabel!
    
    @IBAction func favorite(_ sender: Any) {
        if buttonFav.currentImage == UIImage(named: "heart") {
            buttonFav.setImage(UIImage(named: "heart_selected"), for: .normal)
        } else {
            buttonFav.setImage(UIImage(named: "heart"), for: .normal)
        }
        self.delegate?.favoriteBreedTapped(at: indexPath)
    }
    
    
}
