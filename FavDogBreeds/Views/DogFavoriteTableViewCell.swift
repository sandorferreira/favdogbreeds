//
//  DogFavoriteTableViewCell.swift
//  FavDogBreeds
//
//  Created by Sandor ferreira da silva on 24/01/19.
//  Copyright © 2019 Sandor Ferreira da Silva. All rights reserved.
//

import UIKit

class DogFavoriteTableViewCell: UITableViewCell {

    @IBOutlet weak var breedNameLabel: UILabel!
    
    @IBOutlet weak var favButton: UIButton!
    
}
