//
//  HomeNavBar.swift
//  FavDogBreeds
//
//  Created by Sandor ferreira da silva on 23/01/19.
//  Copyright © 2019 Sandor Ferreira da Silva. All rights reserved.
//

import UIKit

class HomeNavBar: UINavigationBar {
    private let saldoLabel : UILabel
    
    public override init(frame: CGRect) {
        
        var childFrame = CGRect(x: frame.width / 3, y: 0, width: frame.width/3, height: frame.height)
        saldoLabel = UILabel(frame: childFrame)
        saldoLabel.text = "Pawtners"
        saldoLabel.textAlignment = .center
        saldoLabel.font = UIFont.systemFont(ofSize: 18)
        
        super.init(frame: frame)
        self.shadowImage = UIImage()
        self.setBackgroundImage(UIImage(), for: .default)
        addSubview(saldoLabel)
        
    }
    
    @available(*, unavailable)
    public required init?(coder: NSCoder) {
        fatalError("init?(coder:) is not supported")
    }
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
}
