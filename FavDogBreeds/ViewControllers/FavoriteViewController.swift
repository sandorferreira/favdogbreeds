//
//  FavoriteViewController.swift
//  FavDogBreeds
//
//  Created by Sandor ferreira da silva on 24/01/19.
//  Copyright © 2019 Sandor Ferreira da Silva. All rights reserved.
//

import UIKit

class FavoriteViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var favoriteBreeds: [String]?
    
    @IBOutlet weak var tableViewFavorites: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableViewFavorites.delegate = self
        self.tableViewFavorites.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let decodedDogs = UserDefaults.standard.object(forKey: "favoriteDogs") as? Data {
            var favoriteDogs: [String]
            do {
                favoriteDogs = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decodedDogs) as! [String]
                self.favoriteBreeds = favoriteDogs
                self.tableViewFavorites.reloadData()
            } catch {
                
            }
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.favoriteBreeds!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DogFavoriteTableViewCell", for: indexPath) as! DogFavoriteTableViewCell
        let breedName = self.favoriteBreeds![indexPath.row]
        cell.breedNameLabel.text = breedName
        
        if !self.isBreedFavorite(breed: breedName) {
            // checking but unnecessary
            cell.favButton.setImage(UIImage(named: "heart"), for: .normal)
        } else {
            // never enter here
            cell.favButton.setImage(UIImage(named: "heart_selected"), for: .normal)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let breedController = self.storyboard?.instantiateViewController(withIdentifier: "BreedViewController") as! BreedViewController
        let breedName = self.favoriteBreeds![indexPath.row]
        breedController.breedName = breedName
        self.present(breedController, animated: true, completion: nil)
    }
    
    func isBreedFavorite(breed name: String) -> Bool {
        var alreadySavedBreed = false
        if let decodedDogs = UserDefaults.standard.object(forKey: "favoriteDogs") as? Data {
            var favoriteDogs: [String]
            do {
                favoriteDogs = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decodedDogs) as! [String]
                for dog in favoriteDogs {
                    if dog == name {
                        alreadySavedBreed = true
                    }
                }
            } catch {
                print("deu ruim ")
            }
        }
        return alreadySavedBreed
    }

}
