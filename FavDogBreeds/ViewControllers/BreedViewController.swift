//
//  BreedViewController.swift
//  FavDogBreeds
//
//  Created by Sandor ferreira da silva on 24/01/19.
//  Copyright © 2019 Sandor Ferreira da Silva. All rights reserved.
//

import UIKit
import Alamofire

class BreedViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    var breedName: String?
    var breedURLImages: [String]?
    private let itemsPerRow: CGFloat = 3
    private let sectionInsets = UIEdgeInsets(top: 50.0, left: 20.0, bottom: 50.0, right: 20.0)
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var navBar: UINavigationBar!
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator.startAnimating()
        Alamofire.request("https://hidden-crag-71735.herokuapp.com/api/\(self.breedName!)/images", method: .get, encoding: JSONEncoding.default).responseJSON { (response) in
            self.activityIndicator.stopAnimating()
            self.activityIndicator.isHidden = true
            if let result = response.result.value {
                let breedImages = (result as! [String])
                self.breedURLImages = breedImages
                self.collectionView.reloadData()
            } else {
                print(response)
            }
        }
        self.title = breedName
        collectionView.delegate = self
        collectionView.dataSource = self
        navBar.topItem?.title = self.breedName
        
        
    }
    

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.breedURLImages?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DogCollectionViewCell", for: indexPath) as! DogCollectionViewCell
        cell.backgroundColor = .black
        if self.breedURLImages?[indexPath.row] != nil {
            self.getImage(url: URL(string: self.breedURLImages![indexPath.row])!) { (image) in
                cell.dogImageView.image = image
            }
        }
        
        return cell
    }
    
    @IBAction func closeController(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension BreedViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        return CGSize(width: widthPerItem, height: widthPerItem)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
    
    public func getImage(url: URL, handler: @escaping ((UIImage) -> Void)) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                handler(image)
            }
            }.resume()
    }
}
