//
//  ViewController.swift
//  FavDogBreeds
//
//  Created by Sandor ferreira da silva on 22/01/19.
//  Copyright © 2019 Sandor Ferreira da Silva. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, FavoriteButtonDelegate{
    
    @IBOutlet weak var breedTableView: UITableView!
    
    var data = [String: [String]]()
    var breeds = [String]()
    var breedMainImageURL = [String: String]()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        breedTableView.delegate = self
        breedTableView.dataSource = self
        
        
        Alamofire.request("https://hidden-crag-71735.herokuapp.com/api/breeds", method: .get, encoding: JSONEncoding.default).responseJSON { (response) in
            if let result = response.result.value {
                let JSON = result as! [String]
                self.breeds = JSON
                print(self.breeds)
                for breed in JSON {
                Alamofire.request("https://hidden-crag-71735.herokuapp.com/api/\(breed)/images", method: .get, encoding: JSONEncoding.default).responseJSON { (response) in
                    if let result = response.result.value {
                            let breedImages = (result as! [String])
                            self.breedMainImageURL[breed] = breedImages.first
                            self.data[breed] = breedImages
                    } else {
                        print(response)
                    }
                }
                }
                self.breedTableView.reloadData()
                
            } else {
                print(response)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.breeds.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DogTableViewCell", for: indexPath) as! DogTableViewCell
        cell.delegate = self
        cell.indexPath = indexPath
        let breedName = breeds[indexPath.row]
        cell.breedNameLabel.text = breedName
        if self.data[breedName]?.count == nil {
            cell.numberOfPhotosLabel.text = "Calculating..."
        } else {
            cell.numberOfPhotosLabel.text = "\(self.data[breedName]?.count ?? 0) photos"
        }
        
        if self.isBreedFavorite(breed: breedName) {
            cell.buttonFav.setImage(UIImage(named: "heart_selected"), for: .normal)
        } else {
            cell.buttonFav.setImage(UIImage(named: "heart"), for: .normal)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let breedController = self.storyboard?.instantiateViewController(withIdentifier: "BreedViewController") as! BreedViewController
        let breedName = self.breeds[indexPath.row]
        //breedController.breedURLImages = self.data[breedName]
        breedController.breedName = breedName
        self.present(breedController, animated: true, completion: nil)
    }
    
    func favoriteBreedTapped(at index: IndexPath) {
        print("button tapped at \(index.row)")
        if let decodedDogs = UserDefaults.standard.object(forKey: "favoriteDogs") as? Data {
            var favoriteDogs: [String]
            do {
                favoriteDogs = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decodedDogs) as! [String]
                var alreadySavedBreed = false
                for dog in favoriteDogs {
                    if dog == self.breeds[index.row] {
                        alreadySavedBreed = true
                    }
                }
                if alreadySavedBreed {
                    print("removed breed")
                    for (i,dog) in favoriteDogs.enumerated() {
                        if dog == self.breeds[index.row] {
                            favoriteDogs.remove(at: i)
                        }
                    }
                    //favoriteDogs.remove(at: index.row)
                } else {
                    print("saved breed")
                    favoriteDogs.append(self.breeds[index.row])
                }
                
                var encodedData: Data
                do {
                    encodedData = try NSKeyedArchiver.archivedData(withRootObject: favoriteDogs, requiringSecureCoding: false)
                    UserDefaults.standard.set(encodedData, forKey: "favoriteDogs")
                    UserDefaults.standard.synchronize()
                } catch {
                    print("deu ruim ")
                }
            } catch {
                print("deu ruim ")
            }
        } else {
            var favoriteDogs = [String]()
            favoriteDogs.append(self.breeds[index.row])
            var encodedData: Data
            do {
                encodedData = try NSKeyedArchiver.archivedData(withRootObject: favoriteDogs, requiringSecureCoding: false)
                UserDefaults.standard.set(encodedData, forKey: "favoriteDogs")
                UserDefaults.standard.synchronize()
            } catch {
                print("eita")
            }
        }
    }
    
    func isBreedFavorite(breed name: String) -> Bool {
        var alreadySavedBreed = false
        if let decodedDogs = UserDefaults.standard.object(forKey: "favoriteDogs") as? Data {
            var favoriteDogs: [String]
            do {
            favoriteDogs = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decodedDogs) as! [String]
                for dog in favoriteDogs {
                    if dog == name {
                        alreadySavedBreed = true
                    }
                }
            } catch {
            print("deu ruim ")
            }
        }
        return alreadySavedBreed
    }
}

