//
//  DogViewModel.swift
//  FavDogBreeds
//
//  Created by Sandor ferreira da silva on 22/01/19.
//  Copyright © 2019 Sandor Ferreira da Silva. All rights reserved.
//

import Foundation

public class DogViewModel {
    private let dog: Dog
    private var mainImageURL: String?
    
    public init(dog: Dog){
        self.dog = dog
    }
    
    public var mainImage : String? {
        return self.dog.imagesURL.first
    }
}
