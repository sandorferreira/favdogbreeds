//
//  Dog.swift
//  FavDogBreeds
//
//  Created by Sandor ferreira da silva on 22/01/19.
//  Copyright © 2019 Sandor Ferreira da Silva. All rights reserved.
//

import Foundation

public class Dog {
    private let name: String
    public let imagesURL: [String]
    
    public init(name: String, imagesURL: [String]) {
        self.name = name
        self.imagesURL = imagesURL
    }
}
